/* Your JS here. */
// console.log('Hello World!')

//main: calls all functions
scrollNavbarPls();
modal_functionality();
carousel_functionality();
//all functions
window.onscroll = function() {
  scrollNavbarPls();
}

function scrollNavbarPls() {
    let navlist = document.querySelectorAll("nav a");
    scrollPos = window.scrollY;
    navlist.forEach(link => {
      let section = document.querySelector(link.hash);
      if (scrollPos + 250 > section.offsetTop && scrollPos + 250 < section.offsetTop + section.offsetHeight ) {
        link.classList.add("highlighted");
      } else {
        link.classList.remove("highlighted");
      }
    });

    let navbar = document.querySelector("nav");
    // let nav_x = document.querySelectorAll("nav a.highlighted");
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
      navbar.style.padding = "15px 10px";
      navbar.style.fontSize = "18px";
      // navbarstyle.padding = "18px 35px";
      document.getElementsByClassName("highlighted")[0].style.padding="18px 20px";

    } else {
      navbar.style.padding = "30px 10px";
      navbar.style.fontSize = "22px";
      document.getElementsByClassName("highlighted")[0].style.padding="29px 20px";
      // document.querySelectorAll("nav a.highlighted").style.padding = "20px 35px";

    }
}

function modal_functionality(){
  let show_apple_modal = document.getElementById("apple_modal_content");
  let show_bosch_modal = document.getElementById("bosch_modal_content");
  let show_uiuc_modal = document.getElementById("uiuc_modal_content");
  let show_robotics_modal = document.getElementById("robotics_modal_content");

  show_apple_modal.style.display="none";
  show_bosch_modal.style.display="none";
  show_uiuc_modal.style.display="none";
  show_robotics_modal.style.display="none";

  let apple_pic = document.getElementById("Apple_pic");
  let bosch_pic = document.getElementById("Bosch_pic");
  let uiuc_pic = document.getElementById("uiuc_pic");
  let robotics_pic = document.getElementById("robotics_pic");

  let apple_close = document.getElementById("apple_close");
  let bosch_close = document.getElementById("bosch_close");
  let uiuc_close = document.getElementById("uiuc_close");
  let robotics_close = document.getElementById("robotics_close");

  apple_pic.onclick=function(){
    show_apple_modal.style.display="block";
    apple_close.onclick=function(){
      show_apple_modal.style.display="none";
    }
  }
  
  bosch_pic.onclick=function(){
    show_bosch_modal.style.display="block";
    bosch_close.onclick=function(){
      show_bosch_modal.style.display="none";
    }
  }

  uiuc_pic.onclick=function(){
    show_uiuc_modal.style.display="block";
    uiuc_close.onclick=function(){
      show_uiuc_modal.style.display="none";
    }
  }

  robotics_pic.onclick=function(){
    show_robotics_modal.style.display="block";
    robotics_close.onclick=function(){
      show_robotics_modal.style.display="none";
    }
  }

  // window.onclick = function(bg) {
  //   if (bg.target != show_robotics_modal || bg.target != show_apple_modal || bg.target != show_uiuc_modal || bg.target != show_bosch_modal) {
  //     console.log("hi");
  //     if (show_uiuc_modal.style.display != "none") {
  //       show_uiuc_modal.style.display = "none";
  //     } else if (show_apple_modal.style.display != "none") {
  //       show_apple_modal.style.display = "none";
  //     } else if (show_bosch_modal.style.display != "none") {
  //       show_bosch_modal.style.display = "none";
  //     } else if (show_robotics_modal.style.display != "none") {
  //       show_robotics_modal.style.display = "none";
  //     }
  //   }
  //   bg.target.pr
  // }
}

function carousel_functionality(){
  let uiuc_carousel_box = document.getElementById("uiuc-ed");
  let aisb_carousel_box = document.getElementById("aisb-ed");
  let podar_carousel_box = document.getElementById("podar-ed");
  let counter=0;

  aisb_carousel_box.style.display="none";
  podar_carousel_box.style.display="none";

  let right_arrow = document.getElementById("right_button");
  let left_arrow = document.getElementById("left_button");


  right_arrow.onclick=function(){
    if(counter==0){
      uiuc_carousel_box.style.display="none";
      aisb_carousel_box.style.display="block";
      counter++;
    }
    else if(counter==1){
      aisb_carousel_box.style.display="none";
      podar_carousel_box.style.display="block";
      counter++;
    }
    else if(counter==2){
      podar_carousel_box.style.display="none";
      uiuc_carousel_box.style.display="block";
      counter=0;
    }
  }

  left_arrow.onclick=function(){
    if(counter==0){
      uiuc_carousel_box.style.display="none";
      podar_carousel_box.style.display="block";
      counter=2;
    }
    else if(counter==1){
      aisb_carousel_box.style.display="none";
      uiuc_carousel_box.style.display="block";
      counter--;
    }
    else if(counter==2){
      podar_carousel_box.style.display="none";
      aisb_carousel_box.style.display="block";
      counter--;
    }
  }

}
